package com.sodapopsoftware.colors;

/**
 * Contains a color and the number of times it was "incremented" in our redis installation.
 */
public class ColorCount implements Comparable<ColorCount> {

	private Color color;
	private int count;

	public ColorCount(Color color, int count) {
		this.color = color;
		this.count = count;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "ColorCount [color=" + this.color + ", count=" + this.count + "]";
	}

	@Override
	public int compareTo(ColorCount otherColorCount) {
		if (otherColorCount == null || otherColorCount.getColor() == null) {
			return -1;
		}
		return this.color.getName().compareTo(otherColorCount.getColor().getName());
	}
}
