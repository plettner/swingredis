package com.sodapopsoftware.colors;

import com.sodapopsoftware.App;
import com.sodapopsoftware.AppException;

public class ColorThread extends Thread {

	private Color color;
	private int delayInMilliseconds;
	private final App appBean;
	private boolean running = false;

	public ColorThread(App appBean, Color color) {
		setColor(color);
		setDelayInMilliseconds(color.length() * 100);
		this.appBean = appBean;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getColorName() {
		return getColor().name();
	}

	public int getDelayInMilliseconds() {
		return this.delayInMilliseconds;
	}

	public void setDelayInMilliseconds(int delayInMilliseconds) {
		this.delayInMilliseconds = delayInMilliseconds;
	}

	public boolean isRunning() {
		return this.running;
	}

	public void stopRunning() {
		this.running = false;
	}

	@Override
	public void run() {
		this.running = true;
		this.appBean.getRedisValueOpUtils().set(getColorName(), 0);
		try {
			while (isRunning()) {
				this.appBean.getRedisValueOpUtils().increment(getColorName());
				Thread.sleep(getDelayInMilliseconds());
			}
		} catch (final Exception exception) {
			throw new AppException(exception);
		}
	}
}
