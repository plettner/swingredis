package com.sodapopsoftware.colors;

public interface ColorCountsListener {

	/**
	 * Listens for updates about Color Counts. The counts we get are absolute, and not deltas.
	 *
	 * @param colorCounts a list of counts of each color
	 */
	void changeColorCounts(ColorCounts colorCounts);

}
