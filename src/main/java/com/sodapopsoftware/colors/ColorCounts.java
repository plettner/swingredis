package com.sodapopsoftware.colors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ColorCounts implements Iterable<ColorCount> {

	private final List<ColorCount> list = new ArrayList<>();

	public ColorCounts() {
	}

	/**
	 * Returns the n-th ColorCount object in this collection
	 *
	 * @param index specifies which entry in this collection to return
	 * @return the n-th ColorCount object in this collection
	 */
	public ColorCount get(int index) {
		if (index < this.list.size()) {
			return this.list.get(index);
		}
		return null;
	}

	public void add(Color color, int count) {
		this.add(new ColorCount(color, count));
	}

	public void add(ColorCount colorCount) {
		this.list.add(colorCount);
	}

	@Override
	public String toString() {
		return "ColorCounts [list=" + this.list + "]";
	}

	public int size() {
		if (this.list == null) {
			return 0;
		}
		return this.list.size();
	}

	@Override
	public Iterator<ColorCount> iterator() {
		return this.list.iterator();
	}

}
