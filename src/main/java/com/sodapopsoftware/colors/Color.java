package com.sodapopsoftware.colors;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.WordUtils;

public enum Color {

	RED(java.awt.Color.red), WHITE(java.awt.Color.white), BLACK(java.awt.Color.black), BLUE(java.awt.Color.blue),
	GREEN(java.awt.Color.green), CYAN(java.awt.Color.cyan), ORANGE(java.awt.Color.orange),
	YELLOW(java.awt.Color.yellow), PURPLE(java.awt.Color.magenta), PINK(java.awt.Color.pink);

	private java.awt.Color actualColor;

	Color(java.awt.Color actualColor) {
		this.actualColor = actualColor;
	}

	public java.awt.Color getActualColor() {
		return this.actualColor;
	}

	/**
	 * Return the length, in characters, of this color's name.
	 *
	 * @return the length in characters, of this color's name.
	 */
	public int length() {
		return toString().length();
	}

	private static List<String> listOfColors = null;

	public static List<String> getNameList() {
		if (listOfColors == null) {
			// We haven't created this list object yet, so create it now.
			listOfColors = new ArrayList<>();
			for (final Color color : Color.values()) {
				listOfColors.add(color.name());
			}
		}
		return listOfColors;
	}

	public String getName() {
		return WordUtils.capitalizeFully(name());
	}

	/**
	 * Return the "delay" for this color in milliseconds. (We're just generating a value based on the
	 * length in characters of the name.)
	 *
	 * @return the delay in milliseconds for this color
	 */
	public int getDelay() {
		return getName().length() * 1;
	}

	public static Color getForName(String name) {
		if (name == null || name.strip().length() == 0) {
			return null;
		}

		for (final Color color : values()) {
			if (color.name().equalsIgnoreCase(name)) {
				return color;
			}
		}

		// Couldn't find a match. Return null;
		return null;
	}

}
