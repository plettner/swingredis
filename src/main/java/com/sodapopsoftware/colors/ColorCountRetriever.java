package com.sodapopsoftware.colors;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sodapopsoftware.RedisValueOpUtils;

@Service
public class ColorCountRetriever {

	private static final Logger logger = LoggerFactory.getLogger(ColorCountRetriever.class);

	private final RedisValueOpUtils redisValueOpUtils;
	private boolean running = true;
	private final List<ColorCountsListener> listeners = new LinkedList<>();

	public ColorCountRetriever(RedisValueOpUtils redisValuOpUtils) {
		this.redisValueOpUtils = redisValuOpUtils;
	}

	public RedisValueOpUtils getRedisValueOpUtils() {
		return this.redisValueOpUtils;
	}

	public boolean isRunning() {
		return this.running;
	}

	public void stop() {
		this.running = false;
	}

	@Scheduled(fixedRate = 500)
	public void colorCountRetrievalLoop() throws InterruptedException {

		final var map = getRedisValueOpUtils().getMap(Color.getNameList());
		final ColorCounts colorCounts = new ColorCounts();
		for (final String key : map.keySet()) {
			final int count = map.get(key);
			final Color color = Color.getForName(key);
			colorCounts.add(color, count);
		}

		// logger.info(colorCounts.toString());
		notifyAll(colorCounts);
	}

	/**
	 * Adds a class that "listens" to the data that this thread produces
	 *
	 * @param colorCountsListener a class that listens for color counts
	 */
	public void addColorCountsListener(ColorCountsListener colorCountsListener) {
		this.listeners.add(colorCountsListener);
	}

	private void notifyAll(ColorCounts colorCounts) {
		for (final ColorCountsListener listener : this.listeners) {
			listener.changeColorCounts(colorCounts);
		}
	}
}