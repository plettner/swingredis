package com.sodapopsoftware;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class AppInfo {

	private String title;
	private String redisHostName;
	private int redisPort;
	private int repaintDelay;

	public void setTitle(String title) {
		this.title = title;
	}

	public void setRedisHostName(String redisHostName) {
		this.redisHostName = redisHostName;
	}

	public void setRedisPort(int redisPort) {
		this.redisPort = redisPort;
	}

	public void setRepaintDelay(int repaintDelay) {
		this.repaintDelay = repaintDelay;
	}

	public String getTitle() {
		return this.title;
	}

	public String getRedisHostName() {
		return this.redisHostName;
	}

	public int getRedisPort() {
		return this.redisPort;
	}

	public int getRepaintDelay() {
		return this.repaintDelay;
	}

}
