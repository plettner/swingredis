package com.sodapopsoftware;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
public class RedisValueOpUtils {

	// Inject the template into ValueOperations
	@Resource(name = "redisStringIntegerTemplate")
	private ValueOperations<String, Integer> valueOps;

	public RedisValueOpUtils() {

	}

	public Integer get(String key) {
		return this.valueOps.get(key);
	}

	public List<Integer> get(List<String> keys) {
		return this.valueOps.multiGet(keys);
	}

	public Map<String, Integer> getMap(List<String> keys) {
		final List<Integer> values = get(keys);
		final Map<String, Integer> map = new HashMap<>();
		int index = 0;
		for (final String key : keys) {
			map.put(key, values.get(index++));
		}
		return map;
	}

	public void set(String key, Integer value) {
		this.valueOps.set(key, value);
	}

	public Integer increment(String key) {
		return this.valueOps.increment(key, 1L).intValue();
	}

	public Integer decrement(String key) {
		return this.valueOps.increment(key, -1L).intValue();
	}

}
