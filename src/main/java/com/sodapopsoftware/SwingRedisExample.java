package com.sodapopsoftware;

import java.awt.EventQueue;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.sodapopsoftware.colors.Color;
import com.sodapopsoftware.colors.ColorCountRetriever;
import com.sodapopsoftware.colors.ColorThread;

/**
 * Employs Spring Boot to use Swing and Redis.
 *
 * @author <a href="mailto:marvin@plettner.com">Marvin R. Plettner</a><br/>
 */
@EnableAsync
@EnableScheduling
@SpringBootApplication
public class SwingRedisExample {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1648125943762550548L;

	public static void main(String[] args) {

		final var context = new SpringApplicationBuilder(SwingRedisExample.class).headless(false).run(args);
		final App appBean = context.getBean(App.class);

		try {
			EventQueue.invokeAndWait(() -> {
				// This code builds the GUI within the main thread. You can read
				// about the need for this code here:
				// https://stackoverflow.com/questions/22534356/java-awt-eventqueue-invokelater-explained
				appBean.getTabbedView().setVisible(true);
			});
		} catch (final Exception exception) {
			throw new AppException(exception);
		}

		// Let the ColorCountRetriever thread notify the TabbedView of updates
		final ColorCountRetriever colorCountRetriever = context.getBean(ColorCountRetriever.class);
		colorCountRetriever.addColorCountsListener(appBean.getTabbedView());

		for (final Color color : Color.values()) {
			appBean.getRedisValueOpUtils().set(color.name(), 0);
		}

		for (final Color color : Color.values()) {
			new ColorThread(appBean, color).start();
		}

	}
}
