package com.sodapopsoftware;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sodapopsoftware.ui.TabbedView;

@Component
public class App {

	@Autowired
	private AppInfo appInfo;

	@Autowired
	private TabbedView tabbedView;

	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private RedisValueOpUtils redisValueOpUtils;

	public App() {
		// purposely left empty
	}

	public void setAppInfo(AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	public void setTabbedView(TabbedView tabbedView) {
		this.tabbedView = tabbedView;
	}

	public void setRedisUtils(RedisUtils redisUtils) {
		this.redisUtils = redisUtils;
	}

	public void setRedisValueOpUtils(RedisValueOpUtils redisValueOpUtils) {
		this.redisValueOpUtils = redisValueOpUtils;
	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	public TabbedView getTabbedView() {
		return this.tabbedView;
	}

	public RedisUtils getRedisUtils() {
		return this.redisUtils;
	}

	public RedisValueOpUtils getRedisValueOpUtils() {
		return this.redisValueOpUtils;
	}

}