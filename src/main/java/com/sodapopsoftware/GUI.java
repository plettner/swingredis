package com.sodapopsoftware;

import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;

import org.springframework.stereotype.Component;

/**
 * Provides the main entry point code for our user interface.
 *
 * @author <a href="mailto:marvin@plettner.com">Marvin R. Plettner</a><br/>
 */
@Component
public class GUI extends JFrame {

	private static final long serialVersionUID = 2433164486882196674L;

	private AppInfo appInfo;

	public GUI(AppInfo appInfo) {

		init(appInfo);

	}

	private void init(AppInfo appInfo) {

		this.appInfo = appInfo;

		final var quitButton = new JButton("Close");
		quitButton.addActionListener((ActionEvent event) -> {
			System.exit(0);
		});

		createLayout(quitButton);

		setTitle(getAppInfo().getTitle());
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	private void createLayout(JComponent... arg) {

		final var contentPane = getContentPane();
		final var groupLayout = new GroupLayout(contentPane);
		contentPane.setLayout(groupLayout);

		groupLayout.setAutoCreateContainerGaps(true);
		groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup().addComponent(arg[0]));
		groupLayout.setVerticalGroup(groupLayout.createSequentialGroup().addComponent(arg[0]));

	}

}