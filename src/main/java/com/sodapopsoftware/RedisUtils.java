package com.sodapopsoftware;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Some of the core bits of our Redis configuration
 *
 * @author <a href="mailto:marvin@plettner.com">Marvin R. Plettner</a>
 */
@Configuration
public class RedisUtils {

	private AppInfo appInfo;

	public RedisUtils(AppInfo appInfo) {
		setAppInfo(appInfo);
	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	public void setAppInfo(AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {

		final RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration(
				getAppInfo().getRedisHostName(), getAppInfo().getRedisPort());
		final JedisConnectionFactory factory = new JedisConnectionFactory(configuration);
		checkConnectionFactory(factory);
		return factory;

	}

	/**
	 * Returns a redisTemplate suitable for the provided types. </br>
	 * <strong>Note:</strong> I initially tried to put this method in repository classes with the hopes
	 * of having strongly typed versions available for each. Spring couldn't handle this and got into a
	 * circular dependency situation along with the jedisConnectionFactory.
	 *
	 * @return a fully instantiated RedisTemplate
	 */
	@Bean
	public RedisTemplate<String, Integer> redisStringIntegerTemplate() {
		final RedisTemplate<String, Integer> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new GenericToStringSerializer<>(Integer.class));
		return template;
	}

	private void checkConnectionFactory(RedisConnectionFactory factory) {

		try {
			final RedisConnection connection = RedisConnectionUtils.getConnection(factory);
			RedisConnectionUtils.releaseConnection(connection, factory);
		} catch (final Exception exception) {
			throw new AppException(exception);
		}

	}

}
