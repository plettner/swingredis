package com.sodapopsoftware.ui;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.sodapopsoftware.AppInfo;

public class Tab extends JScrollPane implements MouseListener {

	private static final long serialVersionUID = -6199883650055811085L;

	private JTable jTable = null;
	private AppInfo appInfo;
	private TabbedView tabbedView;

	public Tab(AppInfo appInfo) {
		super();
		this.appInfo = appInfo;
	}

	public TabbedView getTabbedView() {
		return this.tabbedView;
	}

	public void setTabbedView(TabbedView tabbedView) {
		this.tabbedView = tabbedView;
	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	public void setAppInfo(AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	public Tab(Component view) {
		super(view);
	}

	protected JTable getJTable() {
		return this.jTable;
	}

	protected void setJTable(JTable table) {
		this.jTable = table;
	}

	@Override
	public void repaint() {
		super.repaint();
		if (getJTable() != null) {
			getJTable().repaint();
		}
	}

	@Override
	public void repaint(long amount) {
		super.repaint(amount);
		if (getJTable() != null) {
			getJTable().repaint(getAppInfo().getRepaintDelay());
		}
	}

	@Override
	public void invalidate() {
		super.invalidate();
		if (getJTable() != null) {
			getJTable().invalidate();
		}
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		System.out.println(event.toString());
	}

	@Override
	public void mouseEntered(MouseEvent event) {
		System.out.println(event.toString());
	}

	@Override
	public void mouseExited(MouseEvent event) {
		System.out.println(event.toString());
	}

	@Override
	public void mousePressed(MouseEvent event) {
		System.out.println(event.toString());
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		System.out.println(event.toString());
	}

}
