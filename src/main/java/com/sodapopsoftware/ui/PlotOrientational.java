package com.sodapopsoftware.ui;

import org.jfree.chart.plot.PlotOrientation;

/**
 * Some of our tabs and panels need to be able to adjust the orientation of the view from vertical
 * to horizontal and back. These methods provide the contract for doing so.
 */
public interface PlotOrientational {

	PlotOrientation getPlotOrientation();

	void setPlotOrientation(PlotOrientation plotOrientation);

}
