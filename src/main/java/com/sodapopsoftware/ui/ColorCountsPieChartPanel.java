package com.sodapopsoftware.ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.DefaultPieDataset;

import com.sodapopsoftware.AppInfo;
import com.sodapopsoftware.colors.ColorCount;
import com.sodapopsoftware.colors.ColorCounts;
import com.sodapopsoftware.colors.ColorCountsListener;

/**
 * Encapsulates the drawing of a pretty pie chart that renders our color counts
 */
public class ColorCountsPieChartPanel extends JPanel implements ColorCountsListener, PlotOrientational {

	private static final long serialVersionUID = -6308715582822701286L;

	private DefaultPieDataset<String> dataset = new DefaultPieDataset<>();
	private final boolean withLegend = false;
	private static final String CHART_TITLE = "Counts by Color";
	private ColorCounts colorCounts;
	private JFreeChart chart;
	private PlotOrientation plotOrientation;

	public ColorCountsPieChartPanel(AppInfo appInfo, PlotOrientation plotOrientation) {
		super();
		init(plotOrientation);
	}

	public ColorCounts getColorCounts() {
		return this.colorCounts;
	}

	public void setColorCounts(ColorCounts colorCounts) {
		this.colorCounts = colorCounts;
	}

	@Override
	public PlotOrientation getPlotOrientation() {
		return this.plotOrientation;
	}

	@Override
	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
	}

	void init(PlotOrientation plotOrientation) {

		setPlotOrientation(plotOrientation);

		if (this.dataset == null) {
			this.dataset = new DefaultPieDataset<>();
		}

		if (this.chart == null) {
			this.chart = ChartFactory.createPieChart3D(CHART_TITLE, this.dataset, this.withLegend, true, false);

		}
	}

	private String getDetailedName(ColorCount colorCount) {
		return String.format("%s (%d)", colorCount.getColor().getName(), colorCount.getCount());
	}

	private synchronized void updateData() {
		if (this.dataset != null) {
			this.dataset.clear();

			final PiePlot<String> plot = (PiePlot<String>) this.chart.getPlot();

			for (final ColorCount colorCount : getColorCounts()) {
				final String name = getDetailedName(colorCount);
				this.dataset.setValue(name, colorCount.getCount());
				plot.setSectionPaint(name, colorCount.getColor().getActualColor());
			}
		}
	}

	@Override
	public synchronized void paint(Graphics graphics) {
		// Draw the pie chart graphics onto the panel's drawing space
		final Graphics2D graphics2D = (Graphics2D) graphics;
		final Rectangle2D chartArea = getVisibleRect();
		this.chart.draw(graphics2D, chartArea, null, null);
	}

	@Override
	public synchronized void changeColorCounts(ColorCounts colorCounts) {
		setColorCounts(colorCounts);
		updateData();
		revalidate();
		repaint();
	}

}
