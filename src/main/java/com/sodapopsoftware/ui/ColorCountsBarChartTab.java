package com.sodapopsoftware.ui;

import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;

import org.jfree.chart.plot.PlotOrientation;

import com.sodapopsoftware.AppInfo;
import com.sodapopsoftware.colors.ColorCounts;
import com.sodapopsoftware.colors.ColorCountsListener;

public class ColorCountsBarChartTab extends Tab implements ColorCountsListener, PlotOrientational {

	private static final long serialVersionUID = 1273241086648284230L;

	private final ColorCountsTablePane tablePane;
	private final ColorCountsBarChartPanel chartPanel;
	private PlotOrientation plotOrientation;

	public ColorCountsBarChartTab(AppInfo appInfo, PlotOrientation plotOrientation) {

		super(appInfo);

		getViewport().setBackground(getBackground());
		// setHorizontalScrollBarPolicy(
		// ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.tablePane = new ColorCountsTablePane(appInfo);
		this.chartPanel = new ColorCountsBarChartPanel(appInfo, plotOrientation);

		final JSplitPane splitter = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.tablePane, this.chartPanel);
		splitter.setDividerLocation(100);
		getViewport().add(splitter);

		setPlotOrientation(plotOrientation);
	}

	@Override
	public void changeColorCounts(ColorCounts colorCounts) {
		this.tablePane.changeColorCounts(colorCounts);
		this.chartPanel.changeColorCounts(colorCounts);
	}

	@Override
	public PlotOrientation getPlotOrientation() {
		return this.plotOrientation;
	}

	@Override
	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
		this.chartPanel.setPlotOrientation(getPlotOrientation());
	}

}
