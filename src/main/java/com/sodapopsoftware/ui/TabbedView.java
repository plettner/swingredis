package com.sodapopsoftware.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.plot.PlotOrientation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sodapopsoftware.AppInfo;
import com.sodapopsoftware.colors.ColorCounts;
import com.sodapopsoftware.colors.ColorCountsListener;

@Component
public class TabbedView extends JFrame implements ActionListener, ChangeListener, ColorCountsListener {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TabbedView.class);

	private static final long serialVersionUID = 2423734348128996204L;

	private static final String TAB1 = "Pie Chart";
	private static final String TAB2 = "Bar Chart";
	private static final String TAB3 = "Color Info";

	private AppInfo appInfo;
	private JTabbedPane tabbedPane = new JTabbedPane();
	private final MenuBar menuBar;
	private final ToolBar toolBar;
	private List<Tab> tabs = new ArrayList<Tab>(4);
	private org.jfree.chart.plot.PlotOrientation plotOrientation = PlotOrientation.HORIZONTAL;

	@Autowired
	public TabbedView(AppInfo appInfo, ToolBar toolBar, MenuBar menuBar) {

		this.appInfo = appInfo;
		this.toolBar = toolBar;
		this.menuBar = menuBar;

		toolBar.setTabbedView(this);
		menuBar.setTabbedView(this);

		init(menuBar, toolBar);
	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	public void setAppInfo(AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	public PlotOrientation getPlotOrientation() {
		return this.plotOrientation;
	}

	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
		if (getTabs() != null) {
			for (final Tab tab : getTabs()) {
				if (tab instanceof PlotOrientational) {
					((PlotOrientational) tab).setPlotOrientation(plotOrientation);
				}
			}
		}
	}

	public PlotOrientation togglePlotOrientation() {
		if (this.plotOrientation.isVertical()) {
			setPlotOrientation(PlotOrientation.HORIZONTAL);
		} else {
			setPlotOrientation(PlotOrientation.VERTICAL);
		}
		return getPlotOrientation();
	}

	private void init(MenuBar menuBar, ToolBar toolBar) {
		setTitle(TAB1 + " | " + getAppInfo().getTitle());
		setSize(500, 500);
		setBackground(Color.gray);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		final JLabel title = new JLabel(getAppInfo().getTitle(), new ImageIcon("money.gif"), SwingConstants.CENTER);
		title.setFont(new Font("Helvetica", Font.BOLD, 24));
		getContentPane().add(title, BorderLayout.NORTH);

		final JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());
		getContentPane().add(topPanel);

		// Add the menu and toolbar we built separately
		setJMenuBar(menuBar.getJMenuBar());
		menuBar.addListener(this);

		topPanel.add(toolBar.getJToolBar(), BorderLayout.NORTH);
		topPanel.add(getTabbedPane());
		toolBar.addListener(this);

		// Add additional tabs to the tabbed pain here
		addTab(TAB1, new ColorCountsTab(getAppInfo(), getPlotOrientation()));
		addTab(TAB2, new ColorCountsBarChartTab(getAppInfo(), getPlotOrientation()));
		addTab(TAB3, new InfoTab(getAppInfo()));

		// For pages that support it, set the initial plot orientation
		for (final Tab tab : getTabs()) {
			if (tab instanceof PlotOrientational) {
				((PlotOrientational) tab).setPlotOrientation(getPlotOrientation());
			}
		}

		getTabbedPane().addChangeListener(this);
	}

	public JTabbedPane getTabbedPane() {
		return this.tabbedPane;
	}

	public void setTabbedPane(JTabbedPane pane) {
		this.tabbedPane = pane;
	}

	List<Tab> getTabs() {
		return this.tabs;
	}

	void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

	void addTab(String title, Tab tab) {
		this.tabs.add(tab);
		getTabbedPane().addTab(title, tab);
	}

	@Override
	public void stateChanged(ChangeEvent event) {
		final Object changed = event.getSource();
		if (changed == this.tabbedPane) {
			final int index = this.tabbedPane.getSelectedIndex();
			setTitle(this.tabbedPane.getTitleAt(index) + " | " + getAppInfo().getTitle());
		}
	}

	@Override
	public void repaint() {
		super.repaint();
		repaintChildren();
	}

	@Override
	public void repaint(int x, int y, int width, int height) {
		super.repaint(x, y, width, height);
		repaintChildren();
	}

	@Override
	public void repaint(long tm, int x, int y, int width, int height) {
		super.repaint(tm, x, y, width, height);
		repaintChildren();
	}

	@Override
	public void repaint(long tm) {
		super.repaint(tm);
		repaintChildren(tm);
	}

	protected void repaintChildren() {
		for (final Tab tab : getTabs()) {
			if (tab != null) {
				tab.repaint();
			}
		}
	}

	protected void repaintChildren(long amount) {
		for (final Tab tab : getTabs()) {
			if (tab != null) {
				tab.repaint(amount);
			}
		}
	}

	@Override
	public void invalidate() {
		super.invalidate();
		for (final Tab tab : getTabs()) {
			if (tab != null) {
				tab.invalidate();
			}
		}
	}

	@Override
	public void changeColorCounts(ColorCounts colorCounts) {
		// logger.info("TabbedView notified: " + colorCounts.toString());

		for (final Tab tab : this.tabs) {
			if (tab instanceof ColorCountsListener) {
				final ColorCountsListener listener = (ColorCountsListener) tab;
				listener.changeColorCounts(colorCounts);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		this.menuBar.actionPerformed(actionEvent);
		this.toolBar.actionPerformed(actionEvent);
	}

	protected void doStartThreads(final ActionEvent event) {
		System.out.println("Start All Threads (not implemented)");
	}

	protected void doPauseThreads(final ActionEvent event) {
		System.out.println("Pause All Threads (not implemented)");
	}

	protected void doAddThread(final ActionEvent event) {
		invalidate();
		repaint(getAppInfo().getRepaintDelay());
		System.out.println("Add a Thread (not implemented)");
	}

	protected void doKillThread(final ActionEvent event) {
		System.out.println("Kill a thread (not implemented)");
	}

	protected void doIncDelay(final ActionEvent event) {
		System.out.println("Increment Delay (not implemented)");
	}

	protected void doDecDelay(final ActionEvent event) {
		System.out.println("Decrement Delay (not implemented)");
	}

	protected void doProperties(final ActionEvent event) {
		togglePlotOrientation();
		System.out.println(getPlotOrientation().toString());
	}

}
