package com.sodapopsoftware.ui;

import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import org.jfree.chart.plot.PlotOrientation;

import com.sodapopsoftware.AppInfo;
import com.sodapopsoftware.colors.ColorCount;
import com.sodapopsoftware.colors.ColorCounts;
import com.sodapopsoftware.colors.ColorCountsListener;

/**
 * Encapsulates the spreadsheet-like color counts table in some of the tabs in this app.
 */
public class ColorCountsTablePane extends ScrollPane implements ColorCountsListener, PlotOrientational {

	private static final long serialVersionUID = 8247986602836479688L;

	private PlotOrientation plotOrientation;

	class TableModel extends AbstractTableModel {

		private static final long serialVersionUID = 3439119795634040725L;

		private final String[] COLUMN_HEADERS = { "Color", "Count" };

		private ColorCounts colorCounts;

		public TableModel(ColorCounts colorCounts) {
			this.colorCounts = colorCounts;
		}

		ColorCounts getColorCounts() {
			return this.colorCounts;
		}

		void setColorCounts(ColorCounts colorCounts) {
			this.colorCounts = colorCounts;
		}

		@Override
		public String getColumnName(int index) {
			if (index >= 0 && index < this.COLUMN_HEADERS.length) {
				return this.COLUMN_HEADERS[index];
			}
			return new StringBuffer("#").append(index).toString();
		}

		@Override
		public int getColumnCount() {
			return this.COLUMN_HEADERS.length;
		}

		@Override
		public int getRowCount() {
			return this.colorCounts.size();
			// return Color.values().length;
		}

		@Override
		public Object getValueAt(int row, int col) {

			final ColorCount colorCount = getColorCounts().get(row);
			if (colorCount == null) {
				return "";
			}

			switch (col) {
			case 0:
				return colorCount.getColor().getName();
			case 1:
				return colorCount.getCount();
			default:
				return "";
			}
		}

		public int getDefaultColumnAlignment(int col) {
			if (col == 0) {
				return SwingConstants.LEFT;
			}
			return SwingConstants.RIGHT;
		}
	}

	public ColorCountsTablePane(AppInfo appInfo) {

		super(appInfo);

		setJTable(new JTable());
		getJTable().setAutoCreateColumnsFromModel(false);
		final TableModel model = new TableModel(new ColorCounts());
		getJTable().setModel(model);

		getViewport().setBackground(getBackground());
		getViewport().add(getJTable());

		DefaultTableCellRenderer renderer = null;
		TableColumn col = null;
		for (int index = 0; index < model.getColumnCount(); index++) {
			renderer = new DefaultTableCellRenderer();
			renderer.setHorizontalAlignment(model.getDefaultColumnAlignment(index));
			col = new TableColumn(index, 100, renderer, null);
			getJTable().addColumn(col);
		}

		final JTableHeader header = getJTable().getTableHeader();
		header.setUpdateTableInRealTime(false);

		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

	@Override
	public PlotOrientation getPlotOrientation() {
		return this.plotOrientation;
	}

	@Override
	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
	}

	@Override
	public void changeColorCounts(ColorCounts colorCounts) {
		if (getJTable().getModel() != null) {
			((TableModel) getJTable().getModel()).setColorCounts(colorCounts);
			revalidate();
			this.repaint();
		}
	}

}
