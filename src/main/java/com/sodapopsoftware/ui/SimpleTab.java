package com.sodapopsoftware.ui;

import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.sodapopsoftware.AppInfo;

public class SimpleTab extends Tab {

	private static final long serialVersionUID = 8394328255079587404L;

	class TableModel extends AbstractTableModel {

		private static final long serialVersionUID = 2005867526594257401L;

		public TableModel() {
		}

		private final String[] COLUMN_HEADERS = { "Thread #", "Requests Handled", "Request ID", "Request Type",
				"State", };

		@Override
		public String getColumnName(int index) {
			if (index >= 0 && index < this.COLUMN_HEADERS.length) {
				return this.COLUMN_HEADERS[index];
			}
			return new StringBuffer("#").append(index).toString();
		}

		@Override
		public int getColumnCount() {
			return this.COLUMN_HEADERS.length;
		}

		@Override
		public int getRowCount() {
			return 10;
		}

		@Override
		public Object getValueAt(int row, int col) {

			switch (col) {
			case 0:

			case 1:

			case 2:

			case 3:

			case 4:

			default:
				return "";
			}
		}

		public int getDefaultColumnAlignment(int col) {
			if (col <= 2) {
				return SwingConstants.CENTER;
			}
			return SwingConstants.LEFT;
		}
	}

	public SimpleTab(AppInfo appInfo) {

		super(appInfo);

		setJTable(new JTable());
		getJTable().setAutoCreateColumnsFromModel(false);
		final TableModel model = new TableModel();
		getJTable().setModel(model);

		getViewport().setBackground(getBackground());
		getViewport().add(getJTable());

		DefaultTableCellRenderer renderer = null;
		TableColumn col = null;
		for (int index = 0; index < model.getColumnCount(); index++) {
			renderer = new DefaultTableCellRenderer();
			renderer.setHorizontalAlignment(model.getDefaultColumnAlignment(index));
			col = new TableColumn(index, 100, renderer, null);
			getJTable().addColumn(col);
		}

		final JTableHeader header = getJTable().getTableHeader();
		header.setUpdateTableInRealTime(false);

		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

}
