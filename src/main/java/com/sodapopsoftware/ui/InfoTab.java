package com.sodapopsoftware.ui;

import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.sodapopsoftware.AppInfo;
import com.sodapopsoftware.colors.Color;

public class InfoTab extends Tab {

	private static final long serialVersionUID = -7958511413374590243L;

	class ColorTableModel extends AbstractTableModel {

		private static final long serialVersionUID = -5682292313924050787L;

		public ColorTableModel() {
		}

		private final String[] COLUMN_HEADERS = { "Color", "Delay (ms)" };

		@Override
		public String getColumnName(int index) {
			if (index >= 0 && index < this.COLUMN_HEADERS.length) {
				return this.COLUMN_HEADERS[index];
			}
			return new StringBuffer("#").append(index).toString();
		}

		@Override
		public int getColumnCount() {
			return this.COLUMN_HEADERS.length;
		}

		@Override
		public int getRowCount() {
			return Color.values().length;
		}

		@Override
		public Object getValueAt(int row, int col) {

			switch (col) {
			case 0:
				return Color.values()[row].getName();

			case 1:
				return Color.values()[row].getDelay();

			default:
				return "";
			}
		}

		public int getDefaultColumnAlignment(int col) {
			if (col == 0) {
				return SwingConstants.LEFT;
			}
			return SwingConstants.RIGHT;
		}
	}

	public InfoTab(AppInfo appInfo) {

		super(appInfo);

		setJTable(new JTable());
		getJTable().setAutoCreateColumnsFromModel(false);
		final ColorTableModel model = new ColorTableModel();
		getJTable().setModel(model);

		getViewport().setBackground(getBackground());
		getViewport().add(getJTable());

		DefaultTableCellRenderer renderer = null;
		TableColumn col = null;
		for (int index = 0; index < model.getColumnCount(); index++) {
			renderer = new DefaultTableCellRenderer();
			renderer.setHorizontalAlignment(model.getDefaultColumnAlignment(index));
			col = new TableColumn(index, 100, renderer, null);
			getJTable().addColumn(col);
		}

		final JTableHeader header = getJTable().getTableHeader();
		header.setUpdateTableInRealTime(false);

		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

}
