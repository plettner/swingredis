package com.sodapopsoftware.ui;

import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import com.sodapopsoftware.AppInfo;
import com.sodapopsoftware.colors.ColorCount;
import com.sodapopsoftware.colors.ColorCounts;
import com.sodapopsoftware.colors.ColorCountsListener;

/**
 * Encapsulates the drawing of a pretty bar chart that renders our color counts
 */
public class ColorCountsBarChartPanel extends JPanel implements ColorCountsListener, PlotOrientational {

	private static final long serialVersionUID = -2945031287648032795L;

	private DefaultCategoryDataset dataset;
	private final boolean withLegend = true;
	static final String CHART_TITLE = "Counts by Color";
	private ColorCounts colorCounts;
	private JFreeChart chart;
	private PlotOrientation plotOrientation;

	public ColorCountsBarChartPanel(AppInfo appInfo, PlotOrientation plotOrientation) {
		super();
		init(plotOrientation);
	}

	public ColorCounts getColorCounts() {
		return this.colorCounts;
	}

	public void setColorCounts(ColorCounts colorCounts) {
		this.colorCounts = colorCounts;
	}

	@Override
	public PlotOrientation getPlotOrientation() {
		return this.plotOrientation;
	}

	@Override
	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
		updateChart(this.plotOrientation);
	}

	void init(PlotOrientation plotOrientation) {

		setPlotOrientation(plotOrientation);

		if (this.dataset == null) {
			this.dataset = new DefaultCategoryDataset();
		}
		if (this.chart == null) {
			updateChart(this.plotOrientation);
		}
	}

	private void updateChart(PlotOrientation plotOrientation) {
		this.chart = ChartFactory.createBarChart(CHART_TITLE, "Color", // Category axis
				"Redis Counts", // Value axis
				this.dataset, plotOrientation, this.withLegend, true, true);
	}

	private String getDetailedName(ColorCount colorCount) {
		return String.format("%s (%d)", colorCount.getColor().getName(), colorCount.getCount());
	}

	private void updateData() {
		if (this.dataset != null) {
			this.dataset.clear();

			final CategoryPlot plot = this.chart.getCategoryPlot();
			final BarRenderer renderer = (BarRenderer) plot.getRenderer();

			int index = 0;
			for (final ColorCount colorCount : getColorCounts()) {
				this.dataset.addValue(colorCount.getCount(), getDetailedName(colorCount), "Color Counts");
				final GradientPaint gradientPaint = new GradientPaint(0.0f, 0.0f,
						colorCount.getColor().getActualColor(), 0.0f, 0.0f, colorCount.getColor().getActualColor());
				renderer.setSeriesPaint(index++, gradientPaint);
			}
		}
	}

	@Override
	public void paint(Graphics graphics) {

		// Now draw the pie chart graphics onto the panel's drawing space
		final Graphics2D graphics2D = (Graphics2D) graphics;
		Rectangle2D chartArea = getVisibleRect();
		this.chart.draw(graphics2D, chartArea, null, null);

		chartArea = null;
	}

	@Override
	public void changeColorCounts(ColorCounts colorCounts) {
		setColorCounts(colorCounts);
		updateData();
		revalidate();
		repaint();
	}

}
