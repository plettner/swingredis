package com.sodapopsoftware.ui;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.sodapopsoftware.AppInfo;

public abstract class ScrollPane extends JScrollPane {

	private static final long serialVersionUID = 74826530124934837L;

	private AppInfo appInfo;
	private JTable jTable;

	public ScrollPane(AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	public JTable getJTable() {
		return this.jTable;
	}

	public void setJTable(JTable jTable) {
		this.jTable = jTable;
	}

	public AppInfo getAppInfo() {
		return this.appInfo;
	}

	public void setAppInfo(AppInfo appInfo) {
		this.appInfo = appInfo;
	}

	public JTable getjTable() {
		return this.jTable;
	}

	public void setjTable(JTable jTable) {
		this.jTable = jTable;
	}

}
