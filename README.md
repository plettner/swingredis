# SwingRedis

This useless little application makes increment requests against a Redis service
and then plots those requests in a pie chart and a bar chart for your viewing 
pleasure.

It demonstrates some understanding of Java, Swing, Spring, and Redis.

## Screenshots

![Pie Chart](images/screenshot-pie-chart.png)
![Pie Chart](images/screenshot-bar-chart-horiz.png)
![Pie Chart](images/screenshot-bar-chart-vertical.png)
![Pie Chart](images/screenshot-info-tab.png)


## Requirements

* Java 15 (might run on lower versions down to Java 10)  Tested with OpenJDK.
* Maven
* Redis
* Docker (Optional for running Redis)

## Running Redis for testing

I run Redis in Docker for convenience.  Assuming Docker is installed on
a Linux system, the following docker command will get Redis running in a 
container:
```
sudo docker run -d --name redis -p 6379:6379 --restart unless-stopped -v /etc/redis/:/data redis redis-server /data
```

## Building

There are two ways to build this project.  The first, obvious way is to install
Maven and Java 15.  Then simply type:

```
mvn clean package
```

The second way allows you to build this project without installing Java 15 or Maven.
Instead, assuming you have Docker installed, you build the project in a container.
The first time you do this, it will download everything it needs to build this
project, including all of the Java dependencies when Maven runs.  But it won't 
change your existing installs.  

To build this project using the provided Docker File, cd into the project directory
and type:

```
sudo docker build -t java-swing-redis .
```


## Running

1.  Start redis as instructed above:
```
sudo docker run -d --name redis -p 6379:6379 --restart unless-stopped -v /etc/redis/:/data redis redis-server /data
```
2.  One can run this application in multiple ways:
    a.  It's possible to start this application with Maven with the following command:
    ```
    mvn spring-boot:run -q
    ```
    b.  You can run the fat jar using Java:
    ```
    java -jar SpringBootSwingExample-1.0.jar
    ```

The application will start up and immediately begin making Redis requests.  The
requests are quite simple:  For each color in the applications enum
`com.sodapopsoftware.colors.Color`, a thread is started that will increment
by one the integer value associated with the name of the color in Redis.  Then
the thread will wait for a length of time relative to the length of the color's
name in letters.  The result is that colors with shorter names will have higher
values associated with them in Redis.  Remember, this is about demonstrating 
ability with the technology, not about providing an actual useful service. :)

The toolbar button will change the orientation of the second tab's bar chart.
It doesn't do anything on the other tabs.


